﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ElegantIRC.IRC;

using System.IO;

namespace ElegantIRC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            IRCClient sock = new IRCClient();
            sock.Nickname = "ElegantIRC";
            sock.Username = "ElegantIRC";
            sock.RealName = "ElegantIRC";
            sock.Connect("irc.freenode.net", 6667);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    class ServerErrorEventArgs : EventArgs
    {
        public string ErrorCode { get; private set; }
        public string Message { get; private set; }
        public ReadOnlyCollection<string> Parameters { get; private set; }

        public ServerErrorEventArgs(string errorCode, string message, ReadOnlyCollection<string> parameters = null)
        {
            this.ErrorCode = errorCode;
            this.Message = message;
            this.Parameters = parameters;
        }
    }
}

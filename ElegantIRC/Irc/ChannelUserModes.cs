﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    enum ChannelUserModes
    {
        /// <summary>
        /// User who has created the specified channel.
        /// </summary>
        Creator = 79,
        /// <summary>
        /// User with operator priveleges in the specified channel.
        /// </summary>
        Operator = 111,
        /// <summary>
        /// User with voice priveleges in the specified channel.
        /// </summary>
        Voice = 118,
    }
}

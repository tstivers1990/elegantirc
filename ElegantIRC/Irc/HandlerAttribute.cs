﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    [AttributeUsage(AttributeTargets.Method)]
    sealed class HandlerAttribute : Attribute
    {
        public string Command { get; private set; }

        public HandlerAttribute(string command)
        {
            this.Command = command.ToUpper();
        }

        public HandlerAttribute(Replies replyId)
        {
            this.Command = ((int)replyId).ToString("000");
        }
    }
}

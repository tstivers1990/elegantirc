﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    partial class IRCClient
    {
        private void sendMessagePass(string pass)
        {
            sock.SendMessage(Message.CreateString("PASS", null, null, pass));
        }

        private void sendMessageNick(string nick)
        {
            sock.SendMessage(Message.CreateString("NICK", null, null, nick));
        }

        private void sendMessageUser(string user, string realname, string mode)
        {
            sock.SendMessage(Message.CreateString("USER", null, realname, user, mode, "*"));
        }

        private void sendMessageOper(string name, string pass)
        {
            sock.SendMessage(Message.CreateString("OPER", null, null, name, pass));
        }

        private void sendMessageUserMode(string nick, string mode)
        {
            sock.SendMessage(Message.CreateString("MODE", null, null, nick, mode));
        }

        /// <summary>
        /// Sends a quit message to the remote server.
        /// </summary>
        /// <param name="quitMessage">The quit message. Null for no message.</param>
        private void sendMessageQuit(string quitMessage)
        {
            sock.SendMessage(Message.CreateString("QUIT", null, quitMessage));
        }

        private void sendMessageSQuit(string server, string comment)
        {
            sock.SendMessage(Message.CreateString("SQUIT", null, comment, server));
        }

        private void sendMessageJoin(string channel, string key = null)
        {
            if (key != null)
            {
                sock.SendMessage(Message.CreateString("JOIN", null, null, channel, key));
            }
            else
            {
                sock.SendMessage(Message.CreateString("JOIN", null, null, channel));
            }
        }

        private void sendMessageJoin(List<ChannelKeyPair> channels)
        {
            List<string> channelsWithoutKeys = new List<string>();
            List<string> channelsWithKeys = new List<string>();
            List<string> keys = new List<string>();
            foreach (ChannelKeyPair ckp in channels)
            {
                if (ckp.HasKey)
                {
                    channelsWithKeys.Add(ckp.Channel);
                    keys.Add(ckp.Key);
                }
                else
                {
                    channelsWithoutKeys.Add(ckp.Channel);
                }
            }
            string channelsParam = "";
            string keysParam = "";
            foreach (string channel in channelsWithKeys)
            {
                channelsParam += channel + ",";
            }
            foreach (string channel in channelsWithoutKeys)
            {
                channelsParam += channel + ",";
            }
            foreach (string key in keys)
            {
                keysParam += key + ",";
            }
            channelsParam = channelsParam.TrimEnd(',');
            keysParam = keysParam.TrimEnd(',');
            sock.SendMessage(Message.CreateString("JOIN", null, null, channelsParam, keysParam));
        }

        /// <summary>
        /// Sends a PART message.
        /// </summary>
        /// <param name="partMessage">The part message. Pass null for this parameter to send no part message.</param>
        /// <param name="channels">The channels to part.</param>
        private void sendMessagePart(string partMessage, params string[] channels)
        {
            string channelsParam = "";
            foreach (string channel in channels)
            {
                channelsParam += channel + ",";
            }
            channelsParam = channelsParam.TrimEnd(',');
            sock.SendMessage(Message.CreateString("PART", null, partMessage, channelsParam));
        }

        /// <summary>
        /// Sends a message to part all channels the user is in.
        /// </summary>
        private void sendMessagePartAll()
        {
            sock.SendMessage(Message.CreateString("JOIN", null, null, "0"));
        }

        private void sendMessageChannelMode(string channel, string modes)
        {
            sock.SendMessage(Message.CreateString("MODE", null, null, channel, modes));
        }

        private void sendMessageChannelMode(string channel, string modes, params string[] modeParams)
        {
            List<string> parameters = new List<string>();
            parameters.Add(channel);
            parameters.Add(modes);
            parameters.AddRange(modeParams);

            sock.SendMessage(Message.CreateString("MODE", null, null, parameters.ToArray()));
        }

        /// <summary>
        /// Sets or requests the topic for the specified channel.
        /// </summary>
        /// <param name="channel">The channel</param>
        /// <param name="topic">The topic. To clear the channel's topic, send an empty string as the parameter. To request the topic pass null for this parameter.</param>
        private void sendMessageTopic(string channel, string topic)
        {
            sock.SendMessage(Message.CreateString("TOPIC", null, topic, channel));
        }

        /// <summary>
        /// Sends the names command.
        /// </summary>
        /// <param name="target">The server that should process the request. Pass null if you don't care.</param>
        /// <param name="channels">The list of channels to get names for. If not specified the server will return all names you can see.</param>
        private void sendMessageNames(string target, params string[] channels)
        {
            string channelsParameter = "";
            if (channels.Length <= 0)
            {
                channelsParameter = null;
            }
            else
            {
                for (int i = 0; i < channels.Length; i++)
                {
                    channelsParameter += channels[i] + ",";
                }
                channelsParameter = channelsParameter.TrimEnd(',');
            }

            string command = "";

            if (target != null)
            {
                if (channelsParameter != null)
                {
                    command = Message.CreateString("NAMES", null, null, channelsParameter, target);
                }
                else
                {
                    command = Message.CreateString("NAMES", null, null, target);
                }
            }
            else
            {
                if (channelsParameter != null)
                {
                    command = Message.CreateString("NAMES", null, null, channelsParameter);
                }
                else
                {
                    command = Message.CreateString("NAMES", null, null);
                }
            }

            sock.SendMessage(command);
        }

        /// <summary>
        /// Sends the list command.
        /// </summary>
        /// <param name="target">The server that should process the request. Pass null if you don't care.</param>
        /// <param name="channels">The list of channels to get names for. If not specified the server will return all names you can see.</param>
        private void sendMessageList(string target, params string[] channels)
        {
            string channelsParameter = "";
            if (channels.Length <= 0)
            {
                channelsParameter = null;
            }
            else
            {
                for (int i = 0; i < channels.Length; i++)
                {
                    channelsParameter += channels[i] + ",";
                }
                channelsParameter = channelsParameter.TrimEnd(',');
            }

            if (target != null && channelsParameter != null)
            {
                sock.SendMessage(Message.CreateString("LIST", null, null, channelsParameter, target));
            }
            else if (target != null)
            {
                sock.SendMessage(Message.CreateString("LIST", null, null, target));
            }
            else if (channelsParameter != null)
            {
                sock.SendMessage(Message.CreateString("LIST", null, null, channelsParameter));
            }
            else
            {
                sock.SendMessage(Message.CreateString("LIST", null, null));
            }
        }

        private void sendMessageInvite(string nick, string channel)
        {
            sock.SendMessage(Message.CreateString("INVITE", null, null, nick, channel));
        }

        /// <summary>
        /// Sends a kick message.
        /// </summary>
        /// <param name="channel">The channel to kick the user(s) from.</param>
        /// <param name="comment">The comment. Pass null for no comment.</param>
        /// <param name="users">The users to kick.</param>
        private void sendMessageKick(string channel, string comment, params string[] users)
        {
            List<string> parameters = new List<string>();
            parameters.Add(channel);
            parameters.AddRange(users);
            sock.SendMessage(Message.CreateString("KICK", null, comment, parameters.ToArray()));
        }

        private void sendMessagePrivMsg(string target, string message)
        {
            sock.SendMessage(Message.CreateString("PRIVMSG", null, message, target));
        }

        private void sendMessageNotice(string target, string message)
        {
            sock.SendMessage(Message.CreateString("NOTICE", null, message, target));
        }

        private void sendMessageMotd(string target = null)
        {
            if (target != null)
            {
                sock.SendMessage(Message.CreateString("MOTD", null, null, target));
            }
            else
            {
                sock.SendMessage(Message.CreateString("MOTD", null, null));
            }
        }

        /// <summary>
        /// Sends LUSERS message.
        /// </summary>
        /// <param name="mask">The hostmask of servers to query. Pass null for no hostmask.</param>
        /// <param name="target">The server that should generate the response. Pass null if you don't care.</param>
        private void sendMessageLUsers(string mask, string target)
        {
            if (mask != null && target != null)
            {
                sock.SendMessage(Message.CreateString("LUSERS", null, null, mask, target));
            }
            else if (mask != null)
            {
                sock.SendMessage(Message.CreateString("LUSERS", null, null, mask));
            }
            else if (target != null)
            {
                sock.SendMessage(Message.CreateString("LUSERS", null, null, target));
            }
            else
            {
                sock.SendMessage(Message.CreateString("LUSERS", null, null));
            }
        }

        private void sendMessageVersion(string target = null)
        {
            if (target != null)
            {
                sock.SendMessage(Message.CreateString("VERSION", null, null, target));
            }
            else
            {
                sock.SendMessage(Message.CreateString("VERSION", null, null));
            }
        }

        /// <summary>
        /// Sends STATS message.
        /// </summary>
        /// <param name="query">The stat to query. Pass null to get all stats.</param>
        /// <param name="target">The target to query. Pass null to query the currently connected server.</param>
        private void sendMessageStats(string query, string target)
        {
            if (query != null && target != null)
            {
                sock.SendMessage(Message.CreateString("STATS", null, null, query, target));
            }
            else if (query != null)
            {
                sock.SendMessage(Message.CreateString("STATS", null, null, query));
            }
            else if (target != null)
            {
                sock.SendMessage(Message.CreateString("STATS", null, null, target));
            }
            else
            {
                sock.SendMessage(Message.CreateString("STATS", null, null));
            }
        }

        /// <summary>
        /// Sends a LINKS message to query all servers that the remote (or specified) server knows about.
        /// </summary>
        /// <param name="remoteServer">The remote server that should handle the query. Pass null if you don't care.</param>
        /// <param name="serverMask">The server mask to look for. Pass null to query all servers.</param>
        private void sendMessageLinks(string remoteServer, string serverMask)
        {
            if (remoteServer != null && serverMask != null)
            {
                sock.SendMessage(Message.CreateString("LINKS", null, null, remoteServer, serverMask));
            }
            else if (remoteServer != null)
            {
                sock.SendMessage(Message.CreateString("LINKS", null, null, remoteServer));
            }
            else if (serverMask != null)
            {
                sock.SendMessage(Message.CreateString("LINKS", null, null, serverMask));
            }
            else
            {
                sock.SendMessage(Message.CreateString("LINKS", null, null));
            }
        }

        private void sendMessageTime(string target = null)
        {
            if (target != null)
            {
                sock.SendMessage(Message.CreateString("TIME", null, null, target));
            }
            else
            {
                sock.SendMessage(Message.CreateString("TIME", null, null));
            }
        }

        private void sendMessageConnect(string target, int port, string remoteServer = null)
        {
            if (remoteServer != null)
            {
                sock.SendMessage(Message.CreateString("CONNECT", null, null, target, port.ToString(), remoteServer));
            }
            else
            {
                sock.SendMessage(Message.CreateString("CONNECT", null, null, target, port.ToString()));
            }
        }

        private void sendMessageTrace(string target = null)
        {
            if (target != null)
            {
                sock.SendMessage(Message.CreateString("TRACE", null, null, target));
            }
            else
            {
                sock.SendMessage(Message.CreateString("TRACE", null, null));
            }
        }

        private void sendMessageAdmin(string target = null)
        {
            if (target != null)
            {
                sock.SendMessage(Message.CreateString("ADMIN", null, null, target));
            }
            else
            {
                sock.SendMessage(Message.CreateString("ADMIN", null, null));
            }
        }

        private void sendMessageInfo(string target = null)
        {
            if (target != null)
            {
                sock.SendMessage(Message.CreateString("INFO", null, null, target));
            }
            else
            {
                sock.SendMessage(Message.CreateString("INFO", null, null));
            }
        }

        private void sendMessageServList(string mask, string type = null)
        {
            if (mask != null && type != null)
            {
                sock.SendMessage(Message.CreateString("SERVLIST", null, null, mask, type));
            }
            else if (mask != null)
            {
                sock.SendMessage(Message.CreateString("SERVLIST", null, null, mask));
            }
            else if (type != null)
            {
                sock.SendMessage(Message.CreateString("SERVLIST", null, null, type));
            }
            else
            {
                sock.SendMessage(Message.CreateString("SERVLIST", null, null));
            }
        }

        private void sendMessageSQuery(string serviceName, string text)
        {
            sock.SendMessage(Message.CreateString("SQUERY", null, null, serviceName, text));
        }

        private void sendMessageWho(string mask, bool operatorOnly = false)
        {
            if (mask != null)
            {
                if (operatorOnly)
                {
                    sock.SendMessage(Message.CreateString("WHO", null, null, mask, "o"));
                }
                else
                {
                    sock.SendMessage(Message.CreateString("WHO", null, null, mask));
                }
            }
            else
            {
                if (operatorOnly)
                {
                    sock.SendMessage(Message.CreateString("WHO", null, null, "o"));
                }
                else
                {
                    sock.SendMessage(Message.CreateString("WHO", null, null));
                }
            }
        }

        /// <summary>
        /// Sends a WHOIS message.
        /// </summary>
        /// <param name="target">The server that should process the query. Pass null if the currently connected server should process the query.</param>
        /// <param name="userMasks">The user masks to query.</param>
        private void sendMessageWhois(string target, params string[] userMasks)
        {
            string userMasksParameter = "";
            for (int i = 0; i < userMasks.Length; i++)
            {
                userMasksParameter += userMasks[i] + ",";
            }
            userMasksParameter = userMasksParameter.TrimEnd(',');
            if (target != null)
            {
                sock.SendMessage(Message.CreateString("WHOIS", null, null, target, userMasksParameter));
            }
            else
            {
                sock.SendMessage(Message.CreateString("WHOIS", null, null, userMasksParameter));
            }
        }

        /// <summary>
        /// Sends a WHOWAS message.
        /// </summary>
        /// <param name="count">The maximum number of previous matches to return. Pass -1 to not specify this parameter.</param>
        /// <param name="target">The server that should process the query. Pass null for the server currently connected.</param>
        /// <param name="nicknames">The nicknames to query. Required even though this method will run without this parameter.</param>
        private void sendMessageWhoWas(int count, string target, params string[] nicknames)
        {
            List<string> parameters = new List<string>();
            parameters.AddRange(nicknames);
            if (count > -1)
            {
                parameters.Add(count.ToString());
            }
            if (target != null)
            {
                parameters.Add(target);
            }
            sock.SendMessage(Message.CreateString("WHOWAS", null, null, parameters.ToArray()));
        }

        private void sendMessageKill(string nick, string comment)
        {
            sock.SendMessage(Message.CreateString("KILL", null, comment, nick));
        }

        /// <summary>
        /// Sends a ping message.
        /// </summary>
        /// <param name="identifier">The ping's identifier.</param>
        /// <param name="target">The server that should process the ping. Pass null if the currently connected server should process it.</param>
        private void sendMessagePing(string identifier, string target = null)
        {
            sock.SendMessage(Message.CreateString("PING", null, target, identifier));
        }

        private void sendMessagePong(string target)
        {
            sock.SendMessage(Message.CreateString("PONG", null, target));
        }

        private void sendMessageAway(string awayMessage = null)
        {
            sock.SendMessage(Message.CreateString("AWAY", null, awayMessage));
        }

        private void sendMessageRehash()
        {
            sock.SendMessage(Message.CreateString("REHASH", null, null));
        }

        private void sendMessageDie()
        {
            sock.SendMessage(Message.CreateString("DIE", null, null));
        }

        private void sendMessageRestart()
        {
            sock.SendMessage(Message.CreateString("RESTART", null, null));
        }

        /// <summary>
        /// Sends a SUMMON message.
        /// </summary>
        /// <param name="user">The user to summon.</param>
        /// <param name="target">The target server. Pass null to attempt to summon the user from the server they are connected to.</param>
        /// <param name="channel">The channel. Pass null to leave this parameter out of the message.</param>
        private void sendMessageSummon(string user, string target, string channel)
        {
            if (target != null && channel != null)
            {
                sock.SendMessage(Message.CreateString("SUMMON", null, null, user, target, channel));
            }
            else if (target != null)
            {
                sock.SendMessage(Message.CreateString("SUMMON", null, null, user, target));
            }
            else if (channel != null)
            {
                sock.SendMessage(Message.CreateString("SUMMON", null, null, user, channel));
            }
            else
            {
                sock.SendMessage(Message.CreateString("SUMMON", null, null, user));
            }
        }

        private void sendMessageUsers(string target = null)
        {
            if (target != null)
            {
                sock.SendMessage(Message.CreateString("USERS", null, null, target));
            }
            else
            {
                sock.SendMessage(Message.CreateString("USERS", null, null));
            }
        }

        private void sendMessageWallops(string message)
        {
            sock.SendMessage(Message.CreateString("WALLOPS", null, message));
        }

        private void sendMessageUserHost(params string[] nicknames)
        {
            sock.SendMessage(Message.CreateString("USERHOST", null, null, nicknames));
        }

        private void sendMessageIsOn(params string[] nicknames)
        {
            sock.SendMessage(Message.CreateString("ISON", null, null, nicknames));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC.ClientSocket
{
    class IRCSocket
    {
        #region Public Fields
        public bool IsConnected
        {
            get
            {
                if (sock != null)
                {
                    return sock.Connected;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region Private Fields
        private TcpClient sock;
        private StreamReader reader;
        private StreamWriter writer;
        #endregion

        #region Events
        public event EventHandler<ConnectedEventArgs> Connected;
        public void OnConnected(ConnectedEventArgs e)
        {
            EventHandler<ConnectedEventArgs> handler = this.Connected;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<DisconnectedEventArgs> Disconnected;
        public void OnDisconnected(DisconnectedEventArgs e)
        {
            EventHandler<DisconnectedEventArgs> handler = this.Disconnected;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<MessageReceivedEventArgs> MessageReceived;
        public void OnMessageReceived(MessageReceivedEventArgs e)
        {
            EventHandler<MessageReceivedEventArgs> handler = this.MessageReceived;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<MessageSentEventArgs> MessageSent;
        public void OnMessageSent(MessageSentEventArgs e)
        {
            EventHandler<MessageSentEventArgs> handler = this.MessageSent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<ErrorEventArgs> Error;
        public void OnError(ErrorEventArgs e)
        {
            EventHandler<ErrorEventArgs> handler = this.Error;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region Constructor/Destructor
        public IRCSocket()
        {

        }

        ~IRCSocket()
        {
            finalizeSock();
        }
        #endregion

        #region Public Methods
        public async void Connect(string host, int port)
        {
            finalizeSock();
            if (port > 65535 || port <= 0)
            {
                throw new ArgumentOutOfRangeException("port", port, "Port must be between 1 and 65535");
            }
            if (host == null)
            {
                throw new ArgumentNullException(host, "Host must not be null");
            }
            try
            {
                sock = new TcpClient();
                await sock.ConnectAsync(host, port);
                if (initializeSock())
                {
                    OnConnected(new ConnectedEventArgs());
                    receiveLoop();
                }
            }
            catch (SocketException e)
            {
                OnError(new ErrorEventArgs(e));
            }
            catch (ObjectDisposedException e)
            {
                OnError(new ErrorEventArgs(e));
            }
        }

        public void Disconnect()
        {
            finalizeSock();
            OnDisconnected(new DisconnectedEventArgs());
        }

        public void SendMessageSynchronously(string m)
        {
            try
            {
                writer.WriteLine(m);
                writer.Flush();
                OnMessageSent(new MessageSentEventArgs(m));
            }
            catch (ObjectDisposedException e)
            {
                OnError(new ErrorEventArgs(e));
            }
            catch (IOException e)
            {
                OnError(new ErrorEventArgs(e));
            }
        }

        public async void SendMessage(string m)
        {
            try
            {
                await writer.WriteLineAsync(m);
                OnMessageSent(new MessageSentEventArgs(m));
            }
            catch (ObjectDisposedException e)
            {
                OnError(new ErrorEventArgs(e));
            }
            catch (InvalidOperationException e)
            {
                OnError(new ErrorEventArgs(e));
            }
        }
        #endregion

        #region Private Methods
        private async void receiveLoop()
        {
            try
            {
                while (sock.Connected)
                {
                    string result = await reader.ReadLineAsync();
                    if (result == null)
                    {
                        break;
                    }
                    OnMessageReceived(new MessageReceivedEventArgs(result));
                }
            }
            catch (ObjectDisposedException e)
            {
                OnError(new ErrorEventArgs(e));
            }
            catch (ArgumentOutOfRangeException e)
            {
                OnError(new ErrorEventArgs(e));
            }
            catch (InvalidOperationException e)
            {
                OnError(new ErrorEventArgs(e));
            }
            finally
            {
                finalizeSock();
                OnDisconnected(new DisconnectedEventArgs());
            }
        }

        private bool initializeSock()
        {
            try
            {
                reader = new StreamReader(sock.GetStream());
                writer = new StreamWriter(sock.GetStream());
                writer.AutoFlush = true;
                return true;
            }
            catch (ArgumentException e)
            {
                OnError(new ErrorEventArgs(e));
                return false;
            }
        }

        private void finalizeSock()
        {
            if (sock != null)
            {
                sock.Close();
            }
            if (reader != null)
            {
                reader.Close();
            }
            if (writer != null)
            {
                writer.Close();
            }
        }
        #endregion
    }
}

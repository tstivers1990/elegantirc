﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    class HandleMessageErrorEventArgs : EventArgs
    {
        public string Message { get; private set; }
        public string Error { get; private set; }

        public HandleMessageErrorEventArgs(string message, string error)
        {
            this.Message = message;
            this.Error = error;
        }
    }
}

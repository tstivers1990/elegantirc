﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    class UnhandledMessageEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public UnhandledMessageEventArgs(string message)
        {
            this.Message = message;
        }
    }
}

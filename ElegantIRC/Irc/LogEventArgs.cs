﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    class LogEventArgs : EventArgs
    {
        public string Source { get; private set; }
        public string Message { get; private set; }

        public LogEventArgs(string source, string message)
        {
            this.Source = source;
            this.Message = message;
        }
    }
}

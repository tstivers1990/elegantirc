﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    class RegisteredEventArgs : EventArgs
    {
        public string WelcomeMessage { get; private set; }

        public RegisteredEventArgs(string welcomeMessage)
        {
            this.WelcomeMessage = welcomeMessage;
        }
    }
}

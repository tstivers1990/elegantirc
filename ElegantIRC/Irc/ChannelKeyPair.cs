﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    class ChannelKeyPair
    {
        public string Channel { get; private set; }
        public string Key { get; private set; }
        public bool HasKey { get; private set; }

        /// <summary>
        /// Creates a ChannelKeyPair with no key.
        /// </summary>
        /// <param name="channel">The channel</param>
        public ChannelKeyPair(string channel)
        {
            this.Channel = channel;
            this.HasKey = false;
        }

        /// <summary>
        /// Creates a ChannelKeyPair with key. Do not use this if you don't want to set a key. Use ChannelKeyPair(string channel) instead.
        /// </summary>
        /// <param name="channel">The channel</param>
        /// <param name="key">The key</param>
        public ChannelKeyPair(string channel, string key)
        {
            this.Channel = channel;
            this.Key = key;
            this.HasKey = true;
        }
    }
}

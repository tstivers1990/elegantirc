﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    class Message
    {
        /// <summary>
        /// The prefix of the message. If there is no prefix this will be null.
        /// </summary>
        public string Prefix { get; private set; }
        /// <summary>
        /// The command of the message. Should not be null.
        /// </summary>
        public string Command { get; private set; }
        /// <summary>
        /// The message parameters.
        /// </summary>
        public List<string> Parameters { get; private set; }
        /// <summary>
        /// The trailing portion of the message. If there is no trailing portion this will be null.
        /// </summary>
        public string Trailing { get; private set; }

        private const char separator = ' ';
        private const string prefixIndicator = ":";
        private const string trailingIndicator = " :";

        /// <summary>
        /// Creates a new Message.
        /// </summary>
        /// <param name="command">The command. Must be set, do not pass null.</param>
        /// <param name="prefix">The prefix of the message. Pass null for no prefix.</param>
        /// <param name="trailing">The trailing portion of the message. Pass null for no trailing portion.</param>
        /// <param name="parameters">The parameters of the message.</param>
        public Message(string command, string prefix, string trailing, params string[] parameters)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command", "Command must not be null.");
            }
            this.Prefix = prefix;
            this.Command = command.ToUpper();
            this.Parameters = parameters.ToList();
            this.Trailing = trailing;
        }

        /// <summary>
        /// Parses the string and creates a new Message from it.
        /// </summary>
        /// <param name="message">The message string.</param>
        public Message(string message)
        {
            this.Parameters = new List<string>();
            int prefixEnd = -1;
            int trailingStart = message.Length;

            // Get the prefix if it exists
            if (message.StartsWith(prefixIndicator))
            {
                prefixEnd = message.IndexOf(separator);
                this.Prefix = message.Substring(1, prefixEnd - 1);
            }

            // Get the trailing parameter if it exists
            if (message.Contains(trailingIndicator))
            {
                trailingStart = message.IndexOf(trailingIndicator);
                this.Trailing = message.Substring(trailingStart + 2);
            }

            string[] commandAndParams = message.Substring(prefixEnd + 1, trailingStart - prefixEnd - 1).Split(separator);

            this.Command = commandAndParams[0].ToUpper();

            if (commandAndParams.Length > 1)
            {
                this.Parameters = commandAndParams.Skip(1).ToList();
            }
        }

        /// <summary>
        /// Returns a string representation of the message.
        /// </summary>
        /// <returns>String representation of the message.</returns>
        public override string ToString()
        {
            return toString(this.Command, this.Prefix, this.Trailing, this.Parameters.ToArray());
        }

        /// <summary>
        /// Create a string message to be sent to the server (without \r\n).
        /// </summary>
        /// <param name="command">The command. Must be set, do not pass null.</param>
        /// <param name="prefix">The prefix of the message. Pass null for no prefix.</param>
        /// <param name="trailing">The trailing portion of the message. Pass null for no trailing portion.</param>
        /// <param name="parameters">The parameters of the message.</param>
        /// <returns>The message to be sent over the network (without \r\n)</returns>
        public static string CreateString(string command, string prefix, string trailing, params string[] parameters)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command", "Command must not be null.");
            }
            return toString(command.ToUpper(), prefix, trailing, parameters);
        }

        private static string toString(string command, string prefix, string trailing, params string[] parameters)
        {
            string result = "";
            if (prefix != null)
            {
                result += prefixIndicator + prefix + separator;
            }
            result += command;
            foreach (string param in parameters)
            {
                result += separator + param;
            }
            if (trailing != null)
            {
                result += trailingIndicator + trailing;
            }
            return result;
        }
    }
}

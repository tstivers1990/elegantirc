﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    class DebugLogEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public DebugLogEventArgs(string message)
        {
            this.Message = message;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml.Linq;

namespace ElegantIRC.IRC
{
    partial class IRCClient
    {
        private Dictionary<string, MessageHandler> handlers;

        private delegate void MessageHandler(Message m);

        [Handler(Replies.RPL_WELCOME)]
        private void handleMessageReplyWelcome(Message m)
        {
            OnRegistered(new RegisteredEventArgs(m.Trailing));
            OnLog(new LogEventArgs(m.Prefix, m.Trailing));
        }

        [Handler(Replies.RPL_YOURHOST)]
        private void handleMessageReplyYourHost(Message m)
        {
            OnLog(new LogEventArgs(m.Prefix, m.Trailing));
        }

        [Handler(Replies.RPL_CREATED)]
        private void handleMessageReplyCreated(Message m)
        {
            OnLog(new LogEventArgs(m.Prefix, m.Trailing));
        }

        [Handler(Replies.RPL_MYINFO)]
        private void handleMessageReplyMyInfo(Message m)
        {
            this.ServerName = m.Parameters[1];
            this.ServerVersion = m.Parameters[2];
            this.AvailableUserModes = m.Parameters[3];
            this.AvailableChannelModes = m.Parameters[4];
            OnLog(new LogEventArgs(m.Prefix, m.Trailing));
        }

        [Handler("400-599")]
        private void handleMessageError(Message m)
        {
            if (m.Parameters.Count > 0)
            {
                OnServerError(new ServerErrorEventArgs(m.Command, m.Trailing, m.Parameters.AsReadOnly()));
            }
            else
            {
                OnServerError(new ServerErrorEventArgs(m.Command, m.Trailing));
            }
        }
    }
}

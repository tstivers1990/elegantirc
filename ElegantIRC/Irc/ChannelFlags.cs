﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantIRC.IRC
{
    enum ChannelFlags
    {
        /// <summary>
        /// Messages sent to the channel will be masked as anonymous.
        /// </summary>
        Anonymous = 97,
        /// <summary>
        /// Only users who have been invited to the channel, or who's mask matches the invite list may join.
        /// </summary>
        InviteOnly = 105,
        /// <summary>
        /// Only operators and members with voice may speak in the channel.
        /// </summary>
        Moderated = 109,
        /// <summary>
        /// Only users who have JOINed the channel may speak in it.
        /// </summary>
        NoSendExternal = 110,
        /// <summary>
        /// JOIN, PART, and NICK messages are not send for this channel.
        /// </summary>
        Quiet = 113,
        /// <summary>
        /// Hides the channel from users who have not JOINed it.
        /// </summary>
        Private = 112,
        /// <summary>
        /// Hides the channel from users who have not JOINed it. TOPIC, LIST, NAMES commands also will not generate a reply.
        /// </summary>
        Secret = 115,
        /// <summary>
        /// When the channel has lost all it's operators for a specified time the server will reop some or all of the channel members.
        /// </summary>
        ReOp = 114,
        /// <summary>
        /// Only operators can set the topic in this channel.
        /// </summary>
        Topic = 116,
        /// <summary>
        /// Joining this channel requires a key.
        /// </summary>
        Key = 107,
        /// <summary>
        /// The channel will only allow a specified number of users at any given time.
        /// </summary>
        Limit = 108,
        /// <summary>
        /// Ban mask for the channel.
        /// </summary>
        Ban = 98,
        /// <summary>
        /// Ban override for the channel. Used to override ban masks.
        /// </summary>
        BanException = 101,
        /// <summary>
        /// Invitation mask for the channel. Users whose mask matches any of the invitation masks set are able to join the channel without an invite.
        /// </summary>
        InviteException = 73,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using ElegantIRC.IRC.ClientSocket;

namespace ElegantIRC.IRC
{
    partial class IRCClient
    {
        #region Properties
        private string password;
        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        private string nickname;
        public string Nickname
        {
            get
            {
                return this.nickname;
            }

            set
            {
                this.nickname = value;
            }
        }

        private string username;
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                this.username = value;
            }
        }

        private string realName;
        public string RealName
        {
            get
            {
                return this.realName;
            }

            set
            {
                this.realName = value;
            }
        }

        public bool OutputDebugLog { get; private set; }

        public string ServerName { get; private set; }

        public string ServerVersion { get; private set; }

        public string AvailableUserModes { get; private set; }

        public string AvailableChannelModes { get; private set; }

        public bool Connected
        {
            get
            {
                return sock.IsConnected;
            }
        }
        #endregion

        #region Events
        public event EventHandler<RegisteredEventArgs> Registered;
        public void OnRegistered(RegisteredEventArgs e)
        {
            EventHandler<RegisteredEventArgs> handler = this.Registered;
            if (handler != null)
            {
                Registered(this, e);
            }
        }

        public event EventHandler<ServerErrorEventArgs> ServerError;
        public void OnServerError(ServerErrorEventArgs e)
        {
            EventHandler<ServerErrorEventArgs> handler = this.ServerError;
            if (handler != null)
            {
                ServerError(this, e);
            }
        }

        public event EventHandler<LogEventArgs> Log;
        public void OnLog(LogEventArgs e)
        {
            EventHandler<LogEventArgs> handler = this.Log;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<DebugLogEventArgs> DebugLog;
        public void OnDebugLog(DebugLogEventArgs e)
        {
            EventHandler<DebugLogEventArgs> handler = this.DebugLog;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<HandleMessageErrorEventArgs> HandleMessageError;
        public void OnHandleMessageError(HandleMessageErrorEventArgs e)
        {
            EventHandler<HandleMessageErrorEventArgs> handler = this.HandleMessageError;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<ParseMessageErrorEventArgs> ParseMessageError;
        public void OnParseMessageError(ParseMessageErrorEventArgs e)
        {
            EventHandler<ParseMessageErrorEventArgs> handler = this.ParseMessageError;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<UnhandledMessageEventArgs> UnhandledMessage;
        public void OnUnhandledMessage(UnhandledMessageEventArgs e)
        {
            EventHandler<UnhandledMessageEventArgs> handler = this.UnhandledMessage;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region Private Fields
        IRCSocket sock;
        #endregion

        #region Event Handlers
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        void sock_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            if (OutputDebugLog)
            {
                OnDebugLog(new DebugLogEventArgs(e.Message));
            }
            Message m;
            try
            {
                m = new Message(e.Message);
            }
            catch (Exception ex)
            {
                OnParseMessageError(new ParseMessageErrorEventArgs(e.Message, ex.Message));
                return;
            }
            if (handlers.ContainsKey(m.Command))
            {
                MessageHandler handler = handlers[m.Command];
                try
                {
                    handler(m);
                }
                catch (Exception ex)
                {
                    OnHandleMessageError(new HandleMessageErrorEventArgs(e.Message, ex.Message));
                }
            }
            else
            {
                OnUnhandledMessage(new UnhandledMessageEventArgs(e.Message));
            }
        }

        void sock_MessageSent(object sender, MessageSentEventArgs e)
        {
            Debug.WriteLine("Sent: " + e.Message);
        }

        void sock_Disconnected(object sender, DisconnectedEventArgs e)
        {

        }

        void sock_Connected(object sender, ConnectedEventArgs e)
        {
            Debug.WriteLine("Connected");
            InitializeConnection();
        }

        void sock_Error(object sender, ErrorEventArgs e)
        {
            Debug.Print("Error: " + e.Exception.Message);
        }
        #endregion

        #region Constructor/Destructor
        public IRCClient(bool outputDebugLog = false)
        {
            this.OutputDebugLog = outputDebugLog;
            handlers = new Dictionary<string, MessageHandler>();
            foreach (MethodInfo method in this.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                HandlerAttribute attribute = Attribute.GetCustomAttribute(method, typeof(HandlerAttribute)) as HandlerAttribute;
                if (attribute != null)
                {
                    MessageHandler handler = (MessageHandler)Delegate.CreateDelegate(typeof(MessageHandler), this, method);
                    string[] commandParts = attribute.Command.Split('-');
                    if (commandParts.Length == 2)
                    {
                        int rangeStart = int.Parse(commandParts[0]);
                        int rangeEnd = int.Parse(commandParts[1]);
                        for (int i = rangeStart; i <= rangeEnd; i++)
                        {
                            if (Enum.IsDefined(typeof(Replies), i))
                            {
                                handlers.Add(i.ToString(), handler);
                            }
                        }
                    }
                    else
                    {
                        handlers.Add(attribute.Command, handler);
                    }
                }
            }

            sock = new IRCSocket();
            sock.Connected += sock_Connected;
            sock.Disconnected += sock_Disconnected;
            sock.MessageReceived += sock_MessageReceived;
            sock.MessageSent += sock_MessageSent;
            sock.Error += sock_Error;
        }

        ~IRCClient()
        {
            sock.Connected -= sock_Connected;
            sock.Disconnected -= sock_Disconnected;
            sock.MessageReceived -= sock_MessageReceived;
            sock.MessageSent -= sock_MessageSent;
            sock.Disconnect();
        }
        #endregion

        #region Public Methods
        public void Connect(string host, int port)
        {
            sock.Connect(host, port);
        }
        #endregion

        #region Private Methods
        private void InitializeConnection()
        {
            if (password != null)
            {
                sendMessagePass(password);
            }
            sendMessageNick(nickname);
            sendMessageUser(username, realName, "a");
        }
        #endregion
    }
}
